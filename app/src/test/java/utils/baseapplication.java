package utils;

import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Application;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.NoCopySpan;

/**
 * Created by RamiN on 10/8/2018.
 */

public class baseapplication extends AppCompatActivity{

    static BaseApplication baseApp;

    public static Typeface typeface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseApp = this;

        typeface=Typeface.createFromAsset(getAssets(),Constants.Appfontname);
    }
}
