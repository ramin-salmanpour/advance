package custome_view;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import utils.baseapplication;

/**
 * Created by RamiN on 10/8/2018.
 */

public class myedittext  extends AppCompatEditText{
    public myedittext(Context context) {
        super(context);
        this.setTypeface(baseapplication.typeface) ;

    }

    public myedittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(baseapplication.typeface) ;
    }
}
