package custome_view;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import utils.baseapplication;

/**
 * Created by RamiN on 10/8/2018.
 */

public class mytextview extends AppCompatTextView {


    public mytextview(Context context) {
        super(context);
        this.setTypeface(baseapplication.typeface);
    }

    public mytextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(baseapplication.typeface);
    }
}
