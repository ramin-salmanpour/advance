package custome_view;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import utils.baseapplication;

/**
 * Created by RamiN on 10/8/2018.
 */

public class mybutton extends AppCompatButton{


    public mybutton(Context context) {
        super(context);
        this.setTypeface(baseapplication.typeface) ;
    }

    public mybutton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(baseapplication.typeface) ;
    }
}
